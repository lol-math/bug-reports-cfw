import { EmailMessage } from "cloudflare:email";
import { createMimeMessage } from "mimetext";
import zod from "zod";

export interface Env {
  SEB: SendEmail;
  from: string;
  to: string;
}

const requestBodySchema = zod.object({
  subject: zod.string().min(1),
  message: zod.string().min(1),
});

const handler: ExportedHandler<Env> = {
  async fetch(request, env, ctx) {
    try {
      const requestBody = await request.json().then(requestBodySchema.parse);

      const msg = createMimeMessage();
      msg.setSender({ name: "Website bugreport form", addr: env.from });
      msg.setRecipient(env.to);
      msg.setSubject(requestBody.subject);
      msg.addMessage({
        contentType: "text/plain",
        data: requestBody.message,
      });

      const message = new EmailMessage(env.from, env.to, msg.asRaw());

      await env.SEB.send(message);

      let response = new Response("", { status: 201 });
      response.headers.set("Access-Control-Allow-Origin", "*");
      response.headers.append("Vary", "Origin");

      return response;
    } catch (e: any) {
      let response = new Response(e.message, { status: 500 });
      response.headers.set("Access-Control-Allow-Origin", "*");
      response.headers.append("Vary", "Origin");

      return response;
    }
  },
};

export default handler;
